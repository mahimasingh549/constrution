import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {AboutPageComponent} from "./about-page/about-page.component";
import {ContactPageComponent} from "./contact-page/contact-page.component";
import {PortfolioPageComponent} from "./portfolio-page/portfolio-page.component";
import {FormsModule} from "@angular/forms";
import {RouterModule} from "@angular/router";
import {NavbarModule} from "./shared/navbar/navbar.module";
import {FooterModule} from "./shared/footer/footer.module";
import {HomeComponent} from './home/home.component';
import {CareerPageComponent} from './career-page/career-page.component';
import {HrComponent} from "./hr/hr.component";
import {NgxSpinnerModule} from "ngx-spinner";
import {HttpClientModule} from "@angular/common/http";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";


@NgModule({
  declarations: [
    AppComponent,
    AboutPageComponent,
    ContactPageComponent,
    PortfolioPageComponent,
    HomeComponent,
    CareerPageComponent,
    HrComponent,
  ],
  imports: [
    FormsModule,
    BrowserAnimationsModule,
    RouterModule,
    NavbarModule,
    FooterModule,
    BrowserModule,
    HttpClientModule,
    NgxSpinnerModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
