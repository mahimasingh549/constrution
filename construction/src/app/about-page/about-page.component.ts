import { Component, OnInit } from '@angular/core';

declare var checkOldData: Function;

@Component({
  selector: 'app-about-page',
  templateUrl: './about-page.component.html',
  styleUrls: ['./about-page.component.css']
})
export class AboutPageComponent implements OnInit {

  constructor() {
    checkOldData();
  }

  ngOnInit() {
    localStorage.setItem('pageName', 'About');
  }

}
