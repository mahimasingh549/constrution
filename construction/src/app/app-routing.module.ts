import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AboutPageComponent} from "./about-page/about-page.component";
import {PortfolioPageComponent} from "./portfolio-page/portfolio-page.component";
import {ContactPageComponent} from "./contact-page/contact-page.component";
import {HomeComponent} from "./home/home.component";
import {CareerPageComponent} from "./career-page/career-page.component";
import {HrComponent} from "./hr/hr.component";


const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full',
  },
  {path: 'home', component: HomeComponent},
  {path: 'about', component: AboutPageComponent},
  {path: 'portfolio', component: PortfolioPageComponent},
  {path: 'contact', component: ContactPageComponent},
  {path: 'career', component: CareerPageComponent},
  {path: 'HR', component: HrComponent},
  {
    path: '**',
    redirectTo: 'home'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})

export class AppRoutingModule {
}
