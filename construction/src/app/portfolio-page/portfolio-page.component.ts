import { Component, OnInit } from '@angular/core';
declare var checkOldData: Function;
@Component({
  selector: 'app-portfolio-page',
  templateUrl: './portfolio-page.component.html',
  styleUrls: ['./portfolio-page.component.css']
})
export class PortfolioPageComponent implements OnInit {

  constructor() {
    checkOldData();
  }

  ngOnInit() {
    localStorage.setItem('pageName', 'Project');
  }

}
