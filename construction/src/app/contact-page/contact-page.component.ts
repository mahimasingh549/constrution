import {Component, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";
import {NgxSpinnerService} from "ngx-spinner";

declare var checkOldData: Function;

@Component({
  selector: 'app-contact-page',
  templateUrl: './contact-page.component.html',
  styleUrls: ['./contact-page.component.css']
})
export class ContactPageComponent implements OnInit {

  userData: any = {};

  constructor(private https: HttpClient, private spinner: NgxSpinnerService) {
    checkOldData();
  }

  ngOnInit() {
    localStorage.setItem('pageName', 'Contact');
  }

  onClick() {
    this.spinner.show();
    const url = `https://us-central1-emailservice-38611.cloudfunctions.net/httpEmail`;
    const text = 'Hello Manan,<br>' + this.userData.firstName + ' ' + this.userData.lastName + '(' + this.userData.email + ')<br> want to ask you something<br>' +
      '<p>' + this.userData.message + '</p>';
    const body = {
      from: this.userData.email,
      email: 'Info@manncorporation.in',
      html: text
    };

    console.log('body', body);
    this.https.post(url, body)
      .toPromise()
      .then(res => {
        console.log(res);
        this.spinner.hide();
        window.location.reload();
      })
      .catch(err => {
        this.userData = {};
        this.spinner.hide();
        window.location.reload();
      });
  }
}
