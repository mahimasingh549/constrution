import { Component, OnInit } from '@angular/core';
declare var checkOldData: Function;
@Component({
  selector: 'app-hr',
  templateUrl: './hr.component.html',
  styleUrls: ['./hr.component.css']
})
export class HrComponent implements OnInit {

  constructor() {
    checkOldData();
  }

  ngOnInit() {
    localStorage.setItem('pageName', 'Human Resource');
  }

}
