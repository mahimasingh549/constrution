import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";

@Component({
  moduleId: module.id,
  selector: 'app-navbar',
  templateUrl: 'navbar.component.html'
})

export class NavbarComponent implements OnInit {

  pageName: any = 'Home';

  constructor(private router: Router) {
  }

  ngOnInit() {
    console.log('localStorage', localStorage.getItem('pageName'));
    console.log('(this.router.url).split("/")[1]', (this.router.url).split("/")[1]);
    this.pageName = (this.router.url).split("/")[1];
    if (localStorage.getItem('pageName') !== null) {
      this.pageName = localStorage.getItem('pageName');
    }
  }

  changeName(name) {
    this.pageName = name;
  }

}
