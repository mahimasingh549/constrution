import {Component, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {NgxSpinnerService} from "ngx-spinner";

declare var uploadFile: Function;
declare var getUrl: Function;
declare var checkOldData: Function;

@Component({
  selector: 'app-career-page',
  templateUrl: './career-page.component.html',
  styleUrls: ['./career-page.component.css']
})
export class CareerPageComponent implements OnInit {

  viewDisplay: any = 'none';
  display: any = 'none';
  userDetails: any = {};
  files: any;
  fileName: any;

  constructor(private https: HttpClient, private spinner: NgxSpinnerService) {
    checkOldData();
  }

  ngOnInit() {
    localStorage.setItem('pageName', 'Career');
  }

  open() {
    this.viewDisplay = 'block';
    document.getElementById('about').scrollIntoView();
    this.display = 'block';
  }

  closeView() {
    this.viewDisplay = 'none';
    this.display = 'none';

  }

  save() {
    this.spinner.show();
    const folderName = new Date().getTime();
    console.log('========================');
    console.log('name', folderName);
    console.log('name', name + this.fileName);
    console.log('========================');
    uploadFile(this.files, folderName, this.fileName, (uploadResult) => {
      console.log('uploadResult', uploadResult);
      getUrl(folderName, (urlResult) => {
        console.log('urlResult', urlResult);
        const url = `https://us-central1-emailservice-38611.cloudfunctions.net/emailWithAttachment`;
        const text = 'Hello Manan,<br>' + this.userDetails.name + '(' + this.userDetails.email + ')<br> <p>Good Day Sir, ' + this.userDetails.name + ' is send you his/her resume with this email. Please notice that. </p> <br>' +
          '<p> Name: ' + this.userDetails.name + '</p>' +
          '<p> Date of birth: ' + this.userDetails.dob + '</p>' +
          '<p> Address: ' + this.userDetails.address + '</p>' +
          '<p> City: ' + this.userDetails.city + '</p>' +
          '<p> Email: ' + this.userDetails.email + '</p>' +
          '<p> Phone Number: ' + this.userDetails.phoneNo + '</p>';
        console.log('text', text);
        const body = {
          from: this.userDetails.email,
          email: 'Info@manncorporation.in',
          html: text,
          attachments: [{ filename: this.fileName, path: urlResult, contentType: 'application/pdf' }],
        };

        console.log('body', body);
        /*this.https.post(url, body)
          .toPromise()
          .then(res => {
            console.log(res);
            this.userDetails = {};
            this.spinner.hide();
            this.display = 'none';
            window.location.reload();
          })
          .catch(err => {
            this.userDetails = {};
            this.spinner.hide();
            this.display = 'none';
            window.location.reload();
          });*/
      });
    });
  }

  uploadFile(e) {
    console.log('e', e.target.files[0]);
    this.files = e.target.files[0];
    this.fileName = this.files.name;
  }
}
